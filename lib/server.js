const data = require('./data');
const express = require('express');
const parser = require('body-parser');

exports.serve = function serve(port) {
  // Création de l'instance d'express
  // Creating the express instance
  const app = express();

  // Liaison de l'instance d'express à un serveur HTTP
  // Linking the express instance to the HTTP server
  const http = require('http').createServer(app);

  // Création de l'instance de socket.io
  // Crating the socket.io instance
  const io = require('socket.io')(http);

  // Configuration d'express
  // Setting up express
  app.set('view engine', 'ejs')
  .use(parser.urlencoded({ extended: false }))
  .use(express.static(__dirname + '/../public'))
  .use('/jquery', express.static(__dirname + '/../node_modules/jquery/dist/'))
  .use('/socket.io', express.static(__dirname + '/../node_modules/socket.io-client/dist/'));

  // Routing
  app.get('/', function (req, res) {
    res.render('notes', { port: port });
  })
  .get('/note/:id', function (req, res) {
    if (req.params.id != undefined) {
      data.getNote(req.params.id, function (err, note) {
          if (err) res.send(err);
          else
            res.render('note', { id: note.id, name: note.name, content: note.content, creationDate: note.creationDate, port: port });
      });
    } else res.render('404');
  })
  .get('/new', function (req, res) {
    res.render('new', { port: port });
  })

  // Post
  app.post('/notes', function (req, res) {
    data.getNotes(function (err, notes) {
      if (err) res.send(err);
      res.send(notes);
    });
  })
  .post('/create', function (req, res) {
    if (req.body.name != undefined && req.body.content != undefined) {
      data.createNote(req.body.name, req.body.content, function (err, created) {
        if (err) res.send(err)
        else res.send(created);
      });
    } else
      res.send('The given arguments aren\'t valid');
  })
  .post('/edit', function (req, res) {
    if (req.body.id != undefined && req.body.name != undefined && req.body.content != undefined) {
      data.editNote(req.body.id, req.body.name, req.body.content, function (err, edited) {
        if (err) res.send(err)
        else res.send(edited);
      });
    } else
      res.send('The given arguments aren\'t valid');
  })
  .post('/delete', function (req, res) {
    data.deleteNote(req.body.id, function (err, deleted) {
      if (err) res.send(err)
      else res.send(deleted);
    });
  });

  // Lancement du serveur
  // Starting the server
  http.listen(port, function (err) {
    if (err) throw err;
    console.log('Server listening on port ' + port);
  });

  // Lancement du socket
  // Starting the socket
  io.on('connection', function (socket) {
    socket.on('addNote', function () {
      socket.emit('addNote');
      socket.broadcast.emit('addNote');
    });
  });
}
