const mysql = require('mysql');

const conn = mysql.createConnection({
  host: 'localhost',
  user: 'NoteTaking',
  password: 'NoteTaking',
  database: 'NoteTaking'
});

exports.getNotes = function getNotes(callback) {

  var notes = [];

  conn.query('SELECT id, name, content, creationDate FROM Note', function (err, rows) {
    if (err) {
      callback('An error occured while fetching the notes !');
      return;
    }
    for (var i = 0; i < rows.length; i++) {
      var note = { id: rows[i].id, name: rows[i].name, content: rows[i].content, creationDate: rows[i].creationDate };
      notes.push(note);
    }
    callback(null, notes);
    return;
  });
}

exports.getNote = function getNote(id, callback) {

  conn.query('SELECT id, name, content, creationDate FROM Note WHERE id = ?', [ id ], function (err, rows) {
    if (err) {
      callback('An error occured while fetching the note !');
      return;
    }
    if (rows.length == 1) {
      callback(null, rows[0]);
      return;
    } else {
      callback('The note does not exist !');
      return;
    }
  });
}

exports.createNote = function createNote(name, content, callback) {

  conn.query('INSERT INTO Note (name, content, creationDate) VALUES (?, ?, DATE(NOW()))', [ name, content ], function (err, res) {
    if (err) {
      callback('An error occured while creating the note !');
      return;
    }
    if (res.affectedRows == 1)
      callback(null, true);
    else
      callback(null, false);
    return;
  });
};

exports.editNote = function createNote(id, name, content, callback) {

  conn.query('UPDATE Note SET name = ?, content = ? WHERE id = ?', [ name, content, id ], function (err, res) {
    if (err) {
      callback('An error occured while editing the note !');
      return;
    }
    if (res.affectedRows == 1)
      callback(null, true);
    else
      callback(null, false);
    return;
  });
};

exports.deleteNote = function createNote(id, callback) {

  conn.query('DELETE FROM Note WHERE id = ?', [ id ], function (err, res) {
    if (err) {
      callback('An error occured while editing the note !');
      return;
    }
    if (res.affectedRows == 1)
      callback(null, true);
    else
      callback(null, false);
    return;
  });
};
