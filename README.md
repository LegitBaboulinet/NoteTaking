# NoteTaking

A simple note JavaScript taking app with Node.js, Express, Socket.io and MySQL.

# Installation

``` bash
cd NotaTaking
npm install
```

Use the script called 'database/structure.sql' to install the MySQL database.

``` bash
mysql -u root -p < structure.sql
```

# Run

**Linux / MacOS**
``` bash
cd NoteTaking
nodejs app
```

**Windows**
``` batch
cd NoteTaking
node app
```

# Use

In your web browser, browse to this url : 'http://localhost:3000/'

**That's it ! :)**