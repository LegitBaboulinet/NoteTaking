$( document ).ready(function () {

  // Closing messages
  $( '.ui.message .close.icon' ).click(function () {
    $( this ).parent().fadeOut('fast');
  });

  // Socket.io
  socket.on('addNote', function () {
    loadNotes();
  });

});
