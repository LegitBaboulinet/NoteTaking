CREATE DATABASE NoteTaking;
USE NoteTaking;

CREATE USER 'NoteTaking'@'localhost' IDENTIFIED BY 'NoteTaking';
GRANT ALL ON NoteTaking.* TO 'NoteTaking'@'localhost';

CREATE TABLE Note (
	ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	content TEXT NOT NULL,
	creationDate DATE NOT NULL
);
